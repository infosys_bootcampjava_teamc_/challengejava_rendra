import java.util.Scanner;

public class KalkulatorBangunRuang {
    static Scanner scan = new Scanner(System.in);

    public static void menu(){
        String ulangi;
        System.out.println("+--------------------------------+");
        System.out.println("|   Kalkulator luas dan Volume   |");
        System.out.println("+--------------------------------+");
        System.out.println(" Menu ");
        System.out.println(" 1. Hitung Luas Bidang ");
        System.out.println(" 2. Hitung Volume ");
        System.out.println(" 0. Tutup Aplikasi ");
        System.out.println("--------------------------------");
        System.out.print(" Masukkan Pilihan Kamu : ");
        String pilihan = scan.nextLine();

        switch (pilihan){

            case "1":
                do {
                    System.out.println("+---------------------------------+");
                    System.out.println("|          Menghitung Luas        |");
                    System.out.println("+---------------------------------+");
                    System.out.println(" 1. Persegi ");
                    System.out.println(" 2. Lingkaran ");
                    System.out.println(" 3. Segitiga ");
                    System.out.println(" 4. Persegi Panjang ");
                    System.out.println(" 0. Kembali Ke Halaman Pertama ");
                    System.out.println("--------------------------------");
                    System.out.print(" Pilih Bidang Nomor Berapa? : ");
                    pilihan = scan.nextLine();

                    switch (pilihan){
                        case "1":
                            Persegi.luasPersegi();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "2":
                            Lingkaran.luasLingkaran();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "3":
                            Segitiga.luasSegitiga();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "4":
                            PersegiPanjang.luasPersegiPanjang();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "0":
                            menu();
                            break;
                        default:
                            //System.exit(0);
                            System.out.println("-----------------------------");
                            System.out.println(" Pilihnya 0-4 aja yah ");
                    }
                } while (!"4".equals(pilihan));
                break;
            case "2":
                do {
                    System.out.println("+---------------------------------------+");
                    System.out.println("|           Menghitung Volume           |");
                    System.out.println("+---------------------------------------+");
                    System.out.println(" 1. Kubus ");
                    System.out.println(" 2. Balok ");
                    System.out.println(" 3. Tabung ");
                    System.out.println(" 0. Kembali Ke Halaman Pertama ");
                    System.out.println("---------------------------------------");
                    System.out.print(" Masukkan Pilihan Kamu Ya : ");
                    pilihan = scan.nextLine();
                    switch (pilihan){
                        case "1":
                            Kubus.volumeKubus();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "2":
                            Balok.volumeBalok();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "3":
                            Tabung.volumeTabung();
                            System.out.println(" Kembali Ke Menu Sebelumnya ? Ketik [Y] : ");
                            System.out.println(" Press Any Key To Close ");
                            System.out.print(" ");
                            ulangi = scan.nextLine();
                            if("Y".equals(ulangi) == "y".equals(ulangi)){
                                System.exit(0);
                            }
                            break;
                        case "0":
                            menu();
                            break;
                        default:
                            //System.exit(0);
                            System.out.println(" Pilihnya 0-3 aja yah ");
                    }
                } while (!"3".equals(pilihan));
                break;
            case "0":
                System.out.println("-------------------------------------------");
                System.out.println(" Terimakasih Sudah Menggunakan Kalkulator :)");
                System.exit(0);
                break;
            default:
                System.out.println("-------------------------------------------");
                System.out.println(" Kamu Salah Pilih! :(");
                menu();
        }
    }

    public static void main(String[] args) {
        menu();

    }
}
