import java.util.Scanner;

public class Segitiga {
    static void luasSegitiga(){
        Scanner scan = new Scanner(System.in);

        int alas, tinggi;
        double luas;

        System.out.println("+---------------------------------+");
        System.out.println("|       Kamu Memilih Segitiga     |");
        System.out.println("+---------------------------------+");
        System.out.println(" Rumus Luas Segitiga Adalah");
        System.out.println(" Luas = 1/2 x Alas x Tinggi");
        System.out.println("+---------------------------------+");
        System.out.print(" Masukkan Alas   = ");
        alas = scan.nextInt();
        System.out.print(" Masukkan Tinggi = ");
        tinggi = scan.nextInt();
        luas = 0.5 * alas * tinggi;
        System.out.println("---------------------------------");
        System.out.println(" Luas segitiga adalah : " + luas);
        System.out.println("---------------------------------");
    }
}
