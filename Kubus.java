import java.util.Scanner;

public class Kubus {
    static void volumeKubus(){
        Scanner scan = new Scanner(System.in);

        int sisi , volume;

        System.out.println("+---------------------------------+");
        System.out.println("|         Kamu Memilih Kubus      |");
        System.out.println("+---------------------------------+");
        System.out.println(" Rumus Volume Kubus Adalah");
        System.out.println(" Volume = Sisi x Sisi x Sisi");
        System.out.println("+---------------------------------+");
        System.out.print(" Masukkan Sisi Kubus   = ");
        sisi = scan.nextInt();
        volume = sisi * sisi * sisi;
        System.out.println("---------------------------------");
        System.out.println(" Volume kubus adalah : " + volume);
        System.out.println("---------------------------------");
    }
}
